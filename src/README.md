![cover](cover.svg)

# Introduction

This book tries to achieve only one thing:

Show you a project go from **nothing** to **OK**.

By nothing I mean, no code at all. Not even a fleshed idea of what it does. No
goals, no commitments. Just a vague interest.

And by OK I mean it will work, it will have tests, it will be available to
use, it will be **useful** and be a real thing.

Think of it as a sort of documentary on the beginnings of a rock band, only
instead of rockers there is a single overweight Argentinian dev, and instead
of a band there is a piece of software.

So, not much like a documentary on the beginnings of a rock band.

## Target Audience

To get the most benefit from this book I expect the reader to have a basic
knowledge of the Python programming language. Knowing and
understanding the contents of the official
[Python Tutorial.](https://docs.python.org/3/tutorial/) should be enough to be
able to follow all the code in the book, which has been kept as simple as
possible.

Readers with deeper knowledge of the language or more extensive programming
experience may still benefit from it but will find a lot of the first part
boring.

## Requirements

You can try to follow the book by just reading it but that is probably not the
best idea. Actually running the example code is educational. Modifying it
even more so.

So, you will need a working python interpreter. The examples have only been
tested on Linux. I suppose before calling this book "finished" I will have to
make them work on Windows and/or OS X somehow.

Each one of the larger parts of the book requires a separate development
environment and has a "Chapter 0" about setting it up.

> **Info**
> ### How the book is built
>
> * It's written in markdown
> * The sections with code are fed to a heavily patched version of [pyliterate](https://github.com/bslatkin/pyliterate) and its output is built into a "book" by [gitbook](https://gitbook.io)
> * Code samples are formatted using [chroma](https://github.com/alecthomas/chroma) and a [custom gitbook plugin](https://plugins.gitbook.com/plugin/chroma)
> * The code uses a ton of things, links are provided in the [Dependencies Appendix](dependencies.html)
> * All the code and text for the book is available in a [Gitlab repo](https://gitlab.com/ralsina/boxes-book)
>   and it's published and updated at [Gitlab Pages](https://ralsina.gitlab.io/boxes-book/)

## Conventions

This is a book mostly about code, so how the code is shown is important.

If a function, variable or some other thing is mentioned in the text it will
be displayed in monospaced font, like this: `function_name`.

Most of the time, code will be shown in blocks, which can be just a fragment
of code that is not part of our program:

```python
def dummy():
    foo()
    bar()
```

If it's a single line, it will have no line numbers:

```python
print(dummy())
```

If the code is part of a file which is shipped as an example, then it will have
a title telling you the file name (which links to the file) and the line
numbers will match those of the file. For example, this block shows lines 25 to
28 of a file called boxes.py:

<div class="source_title"><a href="part2/code/lesson1/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&25
    def __repr__(self):
        return 'Box(%s, %s, %s, %s, "%s")' % (
            self.x, self.y, self.w, self.y, self.letter
        )
```

All source code is formatted using [black](https://github.com/ambv/black) for
consistency, and at a width of 72 columns, so it fits in a printed page.

Similar to, but not exactly like code blocks are the ones showing simple
preformatted text:

```text
This is preformatted, but it's not actually code.
```

Those are often used to show the output of a program.

When showing how to run a program, it will show a `$` prompt, and the command
itself will be in bold, while the output of the program will be regular text:

```bash
$ python code/lesson3/boxes.py pride-and-prejudice.txt lesson3.svg

Creating a svg file.
```

When explaining a series of steps on a UI, they will be separated by "▶". For example:

> Go to the file menu ▶ click "Open." ▶ Choose your file


## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span
xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Boxes: Your second
python book</span> by <a xmlns:cc="http://creativecommons.org/ns#"
href="ralsina" property="cc:attributionName" rel="cc:attributionURL">Roberto
Alsina</a> is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />
