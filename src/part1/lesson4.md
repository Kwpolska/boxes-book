# BOXES v0.4

In the [previous lesson](lesson3.run.html) we totally nailed drawing inside
the lines ... horizontally. Let's improve on that by being bidimensional.

The code for the Box class has not changed, so it's not shown.

```python-include-noshow:lesson4.py:1:14
```

But now, instead of a big box, let's have a list of, say, 10 pages (or large
boxes), one below the other, slightly separated.

```python-include:lesson4.py:16:16
```

Of course our layout routine needs improvements to handle overflowing a
page vertically.

```python-include:lesson4.py:18:55
```

We need to change our drawing code to draw more than one page.

```python-include:lesson4.py:57:83
```

And here is the output:

![lesson4.svg](lesson4.svg)

Would this work if the pages are arranged differently? Let's put the pages
side by side instead.

```python-include:lesson4.py:85:87
```

![lesson4_side_by_side.svg](lesson4_side_by_side.svg)

And how about pages of different sizes?

```python-include:lesson4.py:90:97
```

![lesson4_random_sizes.svg](lesson4_random_sizes.svg)

So, we can fill pages and pages with little red squares now. Nice!

How about we make the squares not be all the same width?

```python-include:lesson4.py:99:103
```

This adds "noise" to the width of the boxes, so they are now anything between 0.5 and 1.5 units wide.

![lesson4_random_box_sizes.svg](lesson4_random_box_sizes.svg)

That looks interesting...

----------

Further references:

* Full source code for this lesson <a href="lesson4.py" target="_blank">lesson4.py</a>
* <a href="diffs/lesson3_lesson4.html" target="_blank">Difference with code from last lesson</a>

