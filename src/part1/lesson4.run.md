# BOXES v0.4

In the [previous lesson](lesson3.run.html) we totally nailed drawing inside
the lines ... horizontally. Let's improve on that by being bidimensional.

The code for the Box class has not changed, so it's not shown.



But now, instead of a big box, let's have a list of, say, 10 pages (or large
boxes), one below the other, slightly separated.

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&16
pages = [Box(0, i * 55, 30, 50) for i in range(10)]

```

Of course our layout routine needs improvements to handle overflowing a
page vertically.

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&18
# We add a "separation" constant so you can see the boxes individually
separation = .2


def layout(_boxes):
    # Because we modify the box list, we will work on a copy
    boxes = _boxes[:]
    # We start at page 0
    page = 0
    # The 1st box should be placed in the correct page
    previous = boxes.pop(0)
    previous.x = pages[page].x
    previous.y = pages[page].y
    while boxes:
        # We take the new 1st box
        box = boxes.pop(0)
        # And put it next to the other
        box.x = previous.x + previous.w + separation
        # At the same vertical location
        box.y = previous.y
        # But if it's too far to the right...
        if (box.x + box.w) > (pages[page].x + pages[page].w):
            # We go all the way left and a little down
            box.x = pages[page].x
            box.y = previous.y + previous.h + separation

        # But if we go too far down
        if box.y + box.h > pages[page].y + pages[page].h:
            # We go to the next page
            page += 1
            # And put the box at the top-left
            box.x = pages[page].x
            box.y = pages[page].y

        previous = box


layout(many_boxes)

```

We need to change our drawing code to draw more than one page.

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&57
import svgwrite


def draw_boxes(boxes, fname, size):
    dwg = svgwrite.Drawing(fname, profile="full", size=size)
    # Draw the pages
    for page in pages:
        dwg.add(
            dwg.rect(
                insert=(f"{page.x}cm", f"{page.y}cm"),
                size=(f"{page.w}cm", f"{page.h}cm"),
                fill="lightblue",
            )
        )
    # Draw all the boxes
    for box in boxes:
        dwg.add(
            dwg.rect(
                insert=(f"{box.x}cm", f"{box.y}cm"),
                size=(f"{box.w}cm", f"{box.h}cm"),
                fill="red",
            )
        )
    dwg.save()


draw_boxes(many_boxes, "lesson4.svg", ("100cm", "100cm"))

```

And here is the output:

![lesson4.svg](lesson4.svg)

Would this work if the pages are arranged differently? Let's put the pages
side by side instead.

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&85
pages = [Box(i * 35, 0, 30, 50) for i in range(10)]
layout(many_boxes)
draw_boxes(many_boxes, "lesson4_side_by_side.svg", ("100cm", "50cm"))

```

![lesson4_side_by_side.svg](lesson4_side_by_side.svg)

And how about pages of different sizes?

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&90
from random import randint

pages = [
    Box(i * 35, 0, 30 - randint(0, 10), 50 - randint(0, 30))
    for i in range(10)
]
layout(many_boxes)
draw_boxes(many_boxes, "lesson4_random_sizes.svg", ("100cm", "50cm"))

```

![lesson4_random_sizes.svg](lesson4_random_sizes.svg)

So, we can fill pages and pages with little red squares now. Nice!

How about we make the squares not be all the same width?

<div class='source_title'><a href="code/lesson4.py.html" target="_blank">lesson4.py</a></div>

```python
&&&99
many_boxes = [Box(w=1 + randint(-5, 5) / 10) for i in range(5000)]
layout(many_boxes)
draw_boxes(
    many_boxes, "lesson4_random_box_sizes.svg", ("100cm", "50cm")
)

```

This adds "noise" to the width of the boxes, so they are now anything between 0.5 and 1.5 units wide.

![lesson4_random_box_sizes.svg](lesson4_random_box_sizes.svg)

That looks interesting...

----------

Further references:

* Full source code for this lesson <a href="lesson4.py" target="_blank">lesson4.py</a>
* <a href="diffs/lesson3_lesson4.html" target="_blank">Difference with code from last lesson</a>

