import pytest

import boxes

lipsum = (
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    " Nulla sollicitudin justo id libero pharetra, at vehicula nisi "
    "pharetra. Nullam sit amet porttitor arcu. Duis purus dui, luctus"
    " ut ante sed, varius rhoncus diam. Donec faucibus erat in venenatis"
    " dignissim. Cras vitae faucibus dui, a feugiat elit. Fusce non egestas"
    " velit, nec suscipit erat.\n\n"
)


def test_page_overflow(tmpdir):
    """When laying down text, it should not extend below the page."""

    # Create a normal text layout
    pages = boxes.create_pages((30, 50))
    text = lipsum * 20
    inp = tmpdir.mkdir("sub").join("lipsum.txt")
    inp.write(text)
    text_boxes = boxes.create_text_boxes(inp)

    boxes.layout(text_boxes, pages, 0.05)

    max_y = max(b.y + b.h for b in text_boxes)
    assert max_y < pages[0].h
