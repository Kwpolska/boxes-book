import pytest
import boxes


@pytest.mark.parametrize(
    "letter, position, expected",
    [
        ("\n", 0, True),
        ("\n", 50, True),
        (" ", 0, False),
        (" ", 50, True),
        ("\xad", 0, False),
        ("\xad", 50, True),
    ],
)
def test_newline_is_breaking(letter, position, expected):
    """Newlines break even if not too wide."""
    box = boxes.Box(letter=letter, x=position)
    page = boxes.Box(w=30, h=30)

    assert boxes.is_breaking(box, page) == expected
