import boxes


def test_justify_simple():
    """Test a simple use case."""
    # First setup what we will use, our "test case"
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    page = boxes.Box(w=50, h=50)
    separation = .1

    # Check expected characteristics
    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    # Do the thing
    boxes.justify_row(row, page, separation)

    # Check the expected behavior

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    assert row[-1].x + row[-1].w == page.x + page.w
