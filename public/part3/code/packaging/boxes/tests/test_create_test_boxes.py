import boxes


def test_adds_newline(tmpdir):
    """Test that we add a newline if the input file doesn't end in one."""
    # Create a temporary file with context
    hello = tmpdir.mkdir("sub").join("hello.txt")
    hello.write("hello")
    # No newline
    assert hello.read() == "hello"

    text_boxes = boxes.create_text_boxes(hello)

    # And now we have a newline (and a soft hyphen)
    assert "".join([b.letter for b in text_boxes]) == "hel\xadlo\n"


def test_not_add_newline_if_repeated(tmpdir):
    """Test that we don't add a newline if the input file ends in one."""
    # Create a temporary file with context
    hello = tmpdir.mkdir("sub").join("hello.txt")
    hello.write("hello\n")
    # No newline
    assert hello.read() == "hello\n"

    text_boxes = boxes.create_text_boxes(hello)

    # And now we have a newline (and a soft hyphen)
    assert "".join([b.letter for b in text_boxes]) == "hel\xadlo\n"
