Boxes Changelog
===============

0.14
    * Made Boxes an installable package (``setup.py``)
    * Reorganized code
