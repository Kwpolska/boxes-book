Boxes, a SVG layout engine
==========================

This is **Boxes**.  What is it, exactly?

* An example for a book!
* A text typesetting engine, whose output doesn't look bad!
* A SVG text layout engine!
* Very good at typesetting the first few pages of *Pride and Prejudice*!

What it isn't?

* Something fast or well-optimized.
* The be-all and end-all of typesetting. (That would be LaTeX.)

License is MIT.

Install
-------

::

   pip install boxes

After that, install two external packages that are not on PyPI::

    pip install https://github.com/ldo/harfpy/archive/master.zip#egg=harfbuzz https://gitlab.com/ldo/python_freetype/repository/master/archive.zip#egg=freetype

Usage
-----

After installing, you can use the ``boxes`` command.
Try ``boxes --help`` to find out what Boxes can do for you.

