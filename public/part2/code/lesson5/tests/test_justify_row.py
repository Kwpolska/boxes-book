import pytest

import boxes


def test_justify_simple():
    """Test a simple use case."""
    # First setup what we will use, our "test case"
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    page = boxes.Box(w=50, h=50)
    separation = .1

    # Check expected characteristics
    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    # Do the thing
    boxes.justify_row(row, page, separation)

    # Check the expected behavior

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    assert row[-1].x + row[-1].w == page.x + page.w


def test_justify_with_spaces():
    """Test a simple use case with spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[5].letter = " "
    row[5].stretchy = True
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    # Use approx() because floating point adds a tiny error here
    assert pytest.approx(row[-1].x + row[-1].w) == page.x + page.w
    # The element in position 5 must have absorbed all the slack
    # So is 1 (it's width) + 40 (slack) units wide
    assert row[5].w == 41


@pytest.mark.xfail  # FIXME: justify doesn't handle newlines yet!
def test_justify_ends_with_newline():
    """Test a use case with a newline."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[-1].letter = "\n"
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should NOT be flushed-right
    assert row[-1].x + row[-1].w == 10


def test_justify_trailing_spaces():
    """Test a use case with traling spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[-1].letter = " "
    row[-2].letter = " "
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have lost the 2 trailing spaces
    assert len(row) == 8
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    assert row[-1].x + row[-1].w == page.x + page.w


def test_justify_puts_things_in_a_row():
    """Test a simple use case with spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[5].letter = " "
    row[5].stretchy = True
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    # Use approx() because floating point adds a tiny error here
    assert pytest.approx(row[-1].x + row[-1].w) == page.x + page.w
    # All elements should be separated correctly.
    separations = [
        separation - (row[i].x - (row[i - 1].x + row[i - 1].w))
        for i in range(1, len(row))
    ]
    # Again, floating point is inaccurate
    assert max(separations) < 0.00001
    assert min(separations) > -0.00001
