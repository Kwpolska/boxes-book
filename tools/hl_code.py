#!/usr/bin/env python3
"""Highlight a source file in just the right way we like."""

import os
import shlex
import subprocess
import sys

TEMPLATE = open(
    os.path.join(os.path.dirname(__file__), "hl_code.html")
).read()


def main(in_path, out_path):
    highlighted = subprocess.check_output(
        shlex.split(
            f"chroma -l python {in_path} -f html --html-lines --html-only -s vs"
        )
    ).decode(
        "utf8"
    )
    # To fix the links to CSS, calculate how deep we are in the tree and add the right
    # amount of "../"
    csspref = "../" * in_path.count("/")
    contents = TEMPLATE % {
        "highlighted": highlighted,
        "filename": os.path.basename(in_path),
        "csspref": csspref,
    }
    with open(out_path, "w+") as outf:
        outf.write(contents)


if __name__ == "__main__":
    main(*sys.argv[1:])
